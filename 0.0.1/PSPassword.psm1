##############################################################################
# KeePass Password Database
##############################################################################
 Function get-KeePassPassword                    {
    <#
    .Synopsis
        To retrieve password from keypass database
    .DESCRIPTION
        To retrieve password from keypass database in powershell. Store you passwors in keypass and use them with on single masterpassword

       Author : Mark van de Waarsenburg
       Date   : 30-1-2017
                http://www.cloudcorner.nl/

    .NOTES


    .PARAMETER PathToKeePassFolder
        the path to the Keypass program files folder. Default entry is $env:ProgramFiles(x86)\KeePass Password Safe 2"
    .PARAMETER PathToDB
        The path to the keypass database. Default is de keypass database NewDatabase.kdbx in the root of the script.
    .PARAMETER EntryToFind
        The entry to find. It is the title of the keypass entry.
    .EXAMPLE
        get-KeePassPassword  -PathToKeePassFolder "C:\Program Files (x86)\KeePass Password Safe 2"  -PathToDB ".\NewDatabase.kdbx" -EntryToFind "Sample Entry"
    .EXAMPLE
        get-KeePassPassword  -PathToDB ".\NewDatabase.kdbx" -EntryToFind "Sample Entry"
    .EXAMPLE
        $PathToDB      = "$env:USERPROFILE\Documents\TestDatabase.kdbx"
        $EntryToFind   = "Administrator"

        #Plainpassword from Keypass
        $Passwordfromkeypass = get-KeePassPassword -PathToDB  $PathToDB  -EntryToFind $EntryToFind
        $Passwordfromkeypass.password
        $Passwordfromkeypass.username

    .EXAMPLE
        $PathToDB      = "E:\Keypass\NewDatabase.kdbx"
        $EntryToFind   = "Administrator"

        #secure Password
        $Passwordfromkeypass = (ConvertTo-SecureString ((get-KeePassPassword -PathToDB  $PathToDB  -EntryToFind $EntryToFind).password ) -AsPlainText -Force )
   #>

    [CmdletBinding()]

    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$False)]
        [ValidateNotNull()]
        [ValidateNotNullOrEmpty()]
        $PathToKeePassFolder = "C:\Program Files (x86)\KeePass Password Safe 2" ,

        # Param2 help description        
        [Parameter(Mandatory=$False)]
        [ValidateNotNull()]
        [ValidateNotNullOrEmpty()]
        [String]
        $PathToDB = "$env:USERPROFILE\Documents\TestDatabase.kdbx",

        # Param3 help description
        [Parameter(Mandatory=$True)]
        [ValidateNotNull()]
        [ValidateNotNullOrEmpty()]
        [String]
        $EntryToFind ,

        # Param3 help description
        [Parameter(Mandatory=$True)]
        $KeePassPassword
 
       
    )

    Begin{

            #Load all .NET binaries in the folder
            (Get-ChildItem -recurse $PathToKeePassFolder |             
                Where-Object {($_.Extension -EQ ".dll") -or ($_.Extension -eq ".exe")} | 
                    ForEach-Object { 
                                $AssemblyName = $_.FullName
                                 Try {
                                    [Reflection.Assembly]::LoadFile($AssemblyName) 
                                 } Catch{ }
                            } 
            ) | out-null


            Function Get-PasswordInKeePassDB              {
                <#
                .Synopsis
                    Gets matching EntryToFind in KeePass DB
                .DESCRIPTION
                    Gets matching EntryToFind in KeePass DB using Windows Integrated logon
                .EXAMPLE
                    Example of how to use this cmdlet
                    Get-PasswordInKeePassDB -PathToDB "C:\Powershell\PowerShell.kdbx" -EntryToFind "MasterPassword"
                #>



                [CmdletBinding()]
                [OutputType([String[]])]

                param(                
                    # Path To password DB
                    $PathToDB = "E:\Keypass\NewDatabase.kdbx",
                    # Entry to find in DB
                    $EntryToFind = "Administrator",
                    # Password used to open KeePass DB        
                    [Parameter(Mandatory=$true)][String]$PasswordToDB 
                )

                begin{
                    #Load keypass variables and objects
                    $KeypassDatabase       = new-object KeePassLib.PwDatabase
                    $CompositeKey          = new-object KeePassLib.Keys.CompositeKey
                    $CompositeKey.AddUserKey((New-Object KeePassLib.Keys.KcpPassword($PasswordToDB))); 
                    $IOConnectionInfo      = New-Object KeePassLib.Serialization.IOConnectionInfo
                    $IOConnectionInfo.Path = $PathToDB
                    $NULLstatusLogger      = New-Object KeePassLib.Interfaces.NullStatusLogger
                    #open database

                    $KeypassDatabase.Open($IOConnectionInfo,$CompositeKey,$NULLstatusLogger)  
                                              
                }#endBegin

                process{
                    $PasswordItems = $KeypassDatabase.RootGroup.GetObjects($true, $true)
                    foreach($passwordItem in $PaswwordItems){
                               
                        if ($passwordItem.Strings.ReadSafe("Title") -eq $EntryToFind){
                            $KeypassEntry = New-object -TypeName PSobject -Property @{
                                    "Password" =  $passwordItem.Strings.ReadSafe("Password")
                                    "Username" =  $passwordItem.Strings.ReadSafe("UserName")
                                    "Title"    =  $passwordItem.Strings.ReadSafe("Title")
                                    "URL"      =  $passwordItem.Strings.ReadSafe("URL")
                                    "Notes"      =  $passwordItem.Strings.ReadSafe("Notes")
                            }
                            #$passwordItem.Strings.ReadSafe("Password")
                                
                        }#endIf
                    }#endIf
                                              
                        
                }#endprocess

                End{
                    $KeypassDatabase.Close()  
                    return $KeypassEntry                        
                }#endEnd

   
  

            } #endFunction
            Function Get-PasswordInKeePassDBUsingPassword {
                <#
                .Synopsis
                    Short description
                .DESCRIPTION
                    Long description
                .EXAMPLE
                    Example of how to use this cmdlet
                    GetPasswordInKeePassDBUsingPassword -EntryToFind "domain\username" -PasswordToDB myNonTopSeceretPasswordInClearText
                .EXAMPLE
                    Get password using Integrated logon to get master password and then use that to unlock and find the password in the big one.
                    Get-PasswordInKeePassDBUsingPassword -EntryToFind "domain\username" -PasswordToDB (Get-PasswordInKeePassDB -EntryToFind "MasterPassword")
                #>

                [CmdletBinding()]
                [OutputType([String[]])]

                Param
                (
                    # Path To password DB
                    $PathToDB = "E:\Keypass\NewDatabase.kdbx",
                    # Entry to find in DB
                    $EntryToFind = "Administrator",
                    # Password used to open KeePass DB        
                    [Parameter(Mandatory=$true)][String]$PasswordToDB
                )
               
                begin{
             
                    $KeypassDatabase       = new-object KeePassLib.PwDatabase

                    $CompositeKey          = new-object KeePassLib.Keys.CompositeKey
                    $CompositeKey.AddUserKey((New-Object KeePassLib.Keys.KcpPassword($PasswordToDB))); 
                    $IOConnectionInfo      = New-Object KeePassLib.Serialization.IOConnectionInfo
                    $IOConnectionInfo.Path = $PathToDB
                    $NULLstatusLogger      = New-Object KeePassLib.Interfaces.NullStatusLogger

                    $KeypassDatabase.Open($IOConnectionInfo,$CompositeKey,$NULLstatusLogger)                    
                }


                Process{
                    $PasswordItems = $KeypassDatabase.RootGroup.GetObjects($true, $true)
                        foreach($PasswordItem in $PasswordItems)
                        {
             
                            if ($PasswordItem.Strings.ReadSafe("Title") -eq $EntryToFind)
                            {
                                #$passwordItem.Strings.ReadSafe("Password")
                                $KeypassEntry = New-object -TypeName PSobject -Property @{
                                    "Password" =  $passwordItem.Strings.ReadSafe("Password")
                                    "Username" =  $passwordItem.Strings.ReadSafe("UserName")
                                    "Title"    =  $passwordItem.Strings.ReadSafe("Title")
                                    "URL"      =  $passwordItem.Strings.ReadSafe("URL")
                                    "Notes"    =  $passwordItem.Strings.ReadSafe("Notes")

                            }
                            }
                        }
                }

                End{
                        $KeypassDatabase.Close()
                        $PasswordToDB = $null                       
                            return $KeypassEntry  
                        
                }

               
     

            } #endFunction

    }#endprocess

    Process{
        If(!($keepassPassword)){
                $BSTR                 = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($(read-host -Prompt "Password of KeePASS : " -AsSecureString))
                $Global:keepassPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)            
        }

        $Password      = Get-PasswordInKeePassDBUsingPassword -PathToDB $PathToDB -PasswordToDB $keepassPassword  -EntryToFind $EntryToFind
    
    }#process

    End{
        return $Password  
    }#end

 } #End Function 
 Function set-KeePassPassword                    {
      <#
     .Synopsis
        aanmaken nieuw keypass entry en het wijzgingen van een entry
     .DESCRIPTION
        Long description
     .EXAMPLE
          $PathToDB      = "$env:USERPROFILE\Documents\TestDatabase.kdbx"
          $Entryname     = "Adminmw"
          $EntryUsername = "Adminmw"
          $entryPassword = "DitisEentestWachtwoord"
          set-KeePassPassword -PathToDB $PathToDB -Entryname $Entryname -EntryUsername $EntryUsername -EntryPassword $entryPassword -EntryURL "www.nu.nl" -EntryNotes "testaccount"
          get-KeePassPassword -PathToDB $PathToDB -EntryToFind $EntryToFind  
     .EXAMPLE
        Another example of how to use this cmdlet
     #>
     [CmdletBinding()]
     Param
     (
        # Param1 path to keypass application
        [Parameter(Mandatory=$False)]
        [ValidateNotNull()]
        [ValidateNotNullOrEmpty()]
        $PathToKeePassFolder = "C:\Program Files (x86)\KeePass Password Safe 2",
        # Param2 Keypass database
        [Parameter(Mandatory=$False)]
        [ValidateNotNull()]
        [ValidateNotNullOrEmpty()]
        $PathToDB            =  "$env:USERPROFILE\Documents\TestDatabase.kdbx" , 
        [switch]$force, 
        # Param3 help description
        [Parameter(Mandatory=$True)]
        $keepassPassword = "P@ssw0rd12!",            
       # Param3 Entryname
        [Parameter(Mandatory=$True)]
        [String]$Entryname,
        [String]$EntryUsername,
        [String]$EntryPassword ,
        [String]$EntryURL,
        [String]$EntryNotes 
     )
 
     Begin{
            If($force){
                $BSTR                   = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($(read-host -Prompt "Password of KeePASS database : " -AsSecureString))
                $Global:keepassPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR) 
            }
            If(!($keepassPassword)){
                $BSTR                   = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($(read-host -Prompt "Password of KeePASS : " -AsSecureString))
                $Global:keepassPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)            
            }

            #############################################################################################
            # Connect to keyvault
            #############################################################################################
            $NULLstatusLogger      = New-Object KeePassLib.Interfaces.NullStatusLogger
            $KeypassDatabase       = new-object KeePassLib.PwDatabase
            $CompositeKey          = new-object KeePassLib.Keys.CompositeKey
            $IOConnectionInfo      = New-Object KeePassLib.Serialization.IOConnectionInfo
            $IOConnectionInfo.Path = $PathToDB
    
                $CompositeKey.AddUserKey((New-Object KeePassLib.Keys.KcpPassword($keepassPassword))); 
                $KeypassDatabase.Open($IOConnectionInfo,$CompositeKey,$NULLstatusLogger)   



     }#Begin

     Process{

        #############################################################################################
        # Add new entry to Keyvault
        #############################################################################################
        $general   = $KeypassDatabase.RootGroup.FindGroup($KeypassDatabase.RootGroup.Groups.Uuid[0].UuidBytes,0)
        $NewEntry  = New-Object KeePassLib.PwEntry($general , $true , $true)

        #set entry
        if($Entryname){
            $title    = New-Object KeePassLib.Security.ProtectedString($true , $Entryname)
            $NewEntry.Strings.Set("Title",$title)
        }
        if($EntryUsername){
            $username = New-Object KeePassLib.Security.ProtectedString($true , $EntryUsername)
             $NewEntry.Strings.Set("UserName",$username)
             
        }
        if($EntryPassword){
            $Pass     = New-Object KeePassLib.Security.ProtectedString($true , $EntryPassword)
            $NewEntry.Strings.Set("Password",$pass)
        }
        if($EntryURL){
            $url  = New-Object KeePassLib.Security.ProtectedString($true , $EntryURL)
            $NewEntry.Strings.Set("UserName",$url)
        }
        If($EntryNotes){
            $Notes      = New-Object KeePassLib.Security.ProtectedString($true , $EntryNotes)
             $NewEntry.Strings.Set("UserName",$Notes)
        }
    
        #Add to keypass
        $general.AddEntry($NewEntry,1)
        $KeypassDatabase.Save($IStatusLogger)
        $KeypassDatabase.Close()


     }#Process

     End{

        $KeypassDatabase.Close() 

     }#End

 } #End Function 
 Function Connectto-Keepass                      {

    <#
    .Synopsis
       connect to keeypass
    .DESCRIPTION
       connect to keeypass. ReturnsTrue or false
    .EXAMPLE
        $PathTokeepassDB      = "C:\Users\Markv\OneDrive\Share\KeyVault\KeyvaultBackup.kdbx"
        $BSTR                 = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($(read-host -Prompt "Password of KeePASS ($i/3) : " -AsSecureString))
        $Global:PlainPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR) 
        
         Connectto-Keepass -PlainPassword $PlainPassword -PathTokeepassDB $PathTokeepassDB

    .EXAMPLE
        $i = 0
        do{
            $i++
 
            Write-host "[Error] : Password is invalid"
            $BSTR                 = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($(read-host -Prompt "Password of KeePASS ($i/3) : " -AsSecureString))
            $Global:PlainPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)      
   
   
        }while ((Connectto-Keepass -PlainPassword $PlainPassword -PathTokeepassDB $PathTokeepassDB) -eq $false)

    #>

    [CmdletBinding()]

    Param(
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        $PlainPassword,
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true, Position=1)]
        $PathTokeepassDB
    )

    Begin{
        #Test path to keeypass database
        If(!(Test-path $PathTokeepassDB)){Write-Host "[error] : Keepass database ($PathTokeepassDB) not found" -ForegroundColor Red ; break }  

        Function Load-Keypass{
            $PathToKeePassFolder  = "C:\Program Files (x86)\KeePass Password Safe 2" 
            #Load all .NET binaries in the folder
            (Get-ChildItem -recurse $PathToKeePassFolder |             
                Where-Object {($_.Extension -EQ ".dll") -or ($_.Extension -eq ".exe")} | 
                    ForEach-Object { 
                                $AssemblyName = $_.FullName
                                    Try {
                                    [Reflection.Assembly]::LoadFile($AssemblyName) 
                                    } Catch{ }
                            } 
            ) | out-null

        }#end Function

        Load-Keypass
    
    
    }#Begin

    Process{
    
        $KeypassDatabase       = new-object KeePassLib.PwDatabase
        $CompositeKey          = new-object KeePassLib.Keys.CompositeKey
        $CompositeKey.AddUserKey((New-Object KeePassLib.Keys.KcpPassword($PlainPassword))); 
        $IOConnectionInfo      = New-Object KeePassLib.Serialization.IOConnectionInfo
        $IOConnectionInfo.Path = $PathTokeepassDB
        $NULLstatusLogger      = New-Object KeePassLib.Interfaces.NullStatusLogger

        Try{
            $KeypassDatabase.Open($IOConnectionInfo,$CompositeKey,$NULLstatusLogger)   
            Write-verbose "Connected to keepass" 
            $passwordcheck = $true
        }catch{
            $passwordcheck = $false
        }

       
        #$passwordItems = $KeypassDatabase.RootGroup.GetObjects($true, $true)
        #   foreach($passworditem in $passworditems){
        #    $passwordItem.Strings.ReadSafe("Title")
        #   }

    }#Process

    End{
        return $passwordcheck
    }#end

    

} #End Function 

##############################################################################
# Password and Encryption Functions
##############################################################################
 Function New-AESKey                             {
    <#
     .Synopsis
        New-AESKey  
     .DESCRIPTION
        New-AESKey  
     .EXAMPLE 
        New-AESKey 
     .EXAMPLE 
        New-AESKey -AESKeySize 24       
    #>

    [CmdletBinding()] 

    param(
        [Parameter(Mandatory=$false)]
        [ValidateSet(16,24,32)]
        [int]$AESKeySize = 32
    )

    begin{}

    process{
        # Define AES KEY
          $AESKey = (New-Object Byte[] $AESKeySize)

        # Create Random AES Key in length specified in $Key variable.
          [Security.Cryptography.RNGCryptoServiceProvider]::Create().GetBytes($AESKey)
    }

    end{
        return $AESKey
    }

} #End Function
 Function Get-AESHash                            {
    <#
    .Synopsis
        Get-AESHASH  
    .DESCRIPTION
        Get-AESHASH  will decrypt the AESTokne hash created with tje function New-AESHASH. The token can only decrypt bij the person how has encrypt it.
    .EXAMPLE
        $VaultPath =  "$env:PROGRAMFILES\Hashicorp\Vault" 
        New-AESHASH -VaultPath $VaultPath -AESKeyFile "$VaultPath\config\AESkey.txt" -UnsealKeyXML "$VaultPath\config\UnsealKeys.xml" - AESKeyFileHash "$VaultPath\config\AESTokenHash.txt"  -APIAddress "http://192.168.16.50:8200" 
    #>

    [CmdletBinding()]

    param(
        # Param help description
        [Parameter(Mandatory=$false)]
        [string]$VaultPath      = "$env:PROGRAMFILES\Hashicorp\Vault"     ,  

        # Param help description
        [Parameter(Mandatory=$false)]
        [string]$UnsealKeyXML   = "$VaultPath\config\UnsealKeys.xml" ,

        # Param help description
        [Parameter(Mandatory=$false)]
        [string]$APIAddress     = "http://192.168.16.50:8200" ,

        # Param help description
        [Parameter(Mandatory=$false)]
        [string]$AESKeyFileHash = "$VaultPath\config\AESTokenHash.txt" ,
        [Parameter(Mandatory=$false)]
        [string]$unsealkey
    )

    begin{
        # read Hash from AESKeyFilehash
        $keys        = Import-Clixml -Path $UnsealKeyXML
      
    }

    process{
        # convert AESKey from hash
        if(test-path $AESKeyFilehash){
                $AESKeyHash          = get-content $AESKeyFilehash 
                $SecureStringToBSTR  = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($($AESKeyHash | ConvertTo-SecureString))
                $AESKey              = ([System.Runtime.InteropServices.Marshal]::PtrToStringAuto($SecureStringToBSTR)) -split (" ")   
        }else{
            $AESKey   = get-content "$VaultPath\config\aeskey.txt"
        } 


        if($unsealkey){   
            $unsealkey = Convertfrom-SecureHashAES -Hash $($keys.$unsealkey) -AESKey $AESKey
        }else{
            # Set variables for Windows
            $env:VAULT_ADDR  = $APIAddress 
            $env:VAULT_TOKEN = Convertfrom-SecureHashAES -Hash $($keys.InitialRootToken) -AESKey $AESKey
        }

   
    }

    end{
        # Clean up variables
        If( $SecureStringToBSTR ) { remove-variable -name   SecureStringToBSTR -force }
        If( $AESKey ) { remove-variable -name AESKey -force }
        If( $AESKeyHash ) { remove-variable -name AESKeyHash -force }
        If( $hash ) { remove-variable -name hash -force }
        if($unsealkey){ return $unsealkey }

    }

} #End Function

 Function Convertto-SecureHashAES                {
    <#
     .Synopsis
        Convertto-SecureHashAES   
     .DESCRIPTION
        Convertto-SecureHashAES will convert plain text  to a hash.  
     .EXAMPLE 
         $AESKey     = new-AESKey -AESKeySize 16
         Convertto-SecureHashAES -token "ditiseensecuretoken" -tokenName "UnsealKey1" -AESKey $AESKey 
         
         Name         Hash                                                                                                                                                                                 
         ----         ----                                                                                                                                                                                 
         {UnsealKey1} 76492d1116743f0423413b16050a5345MgB8ADIATDMANA...

    #>

    [CmdletBinding()]     

    Param(
        # Param1 help description
        [Parameter(Mandatory=$true)]
        [string[]]$token ,
        [Parameter(Mandatory=$true)]
        [string[]]$tokenName ,

        # Param3 help description
        [Parameter(Mandatory=$false)]
        $AESKey
     
    )

    begin{
        if(!($AESKey)){$AESKey= new-AESKey }      
    }

    process{
        New-object -TypeName PSObject -Property @{
            AESKey = $AESkey
            Hash   = ConvertFrom-SecureString -SecureString (Convert-PLainpasswordtoSecurestring -token $token) -Key $AESKey
            Name   = $tokenName
        }
    }

    end{
        return $result
    }

} #End Function
 Function Convertfrom-SecureHashAES              {
    <#
     .Synopsis
        Convertfrom-SecureHashAES   
     .DESCRIPTION
        Convertfrom-SecureHashAES will convert the hashed token back to plain text 
     .EXAMPLE 
        $AESKey      = new-AESKey -AESKeySize 16
        $HashedToken = Convertto-SecureHashAES -token "DitIsEenSecureToken" -tokenName "UnsealKey1" -AESKey $AESKey

        Convertfrom-SecureHashAES -Hash $($HashedToken.hash) -AESKey $AESKey
    #>

    [CmdletBinding()]  

    param(
        # Param help description
        [Parameter(Mandatory=$true)]
        $AESKey,
        # Param help description
        [Parameter(Mandatory=$true)]    
        $Hashtoken 
    )

    begin{}

    process{
        $Hashtoken | ConvertTo-SecureString -key $AESKey | 
                ForEach-Object {
                    [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($_))
                }#end
    }

    end{}
} #End Function  
              
 Function Convert-SecurestringtoPLainPassword    {
    <#
     .Synopsis
        Convert-SecurestringtoPLainPassword  
     .DESCRIPTION
        Convert-SecurestringtoPLainPassword 
     .EXAMPLE 
        Convert-SecurestringtoPLainPassword 
    #>

    [CmdletBinding()]  

    Param($secureString)

    Begin{
        # Param help description
        [Parameter(Mandatory=$true)]   
        $SecurePassword = $secureString
    }

    Process{        

            $BSTR          = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword)
            $PlainPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR) 
    
    }

    End{
        return $PlainPassword
    }



} #End Function
 Function Convert-PLainpasswordtoSecurestring    {
     <#
    .Synopsis
       Convert-PLainpasswordtoSecurestring
    .DESCRIPTION
       Convert-PLainpasswordtoSecurestring
    .EXAMPLE
       Convert-PLainpasswordtoSecurestring -password "$password12! -username "lab\administrator"
    #>

    [CmdletBinding()]

    Param(
        [Parameter(Mandatory=$true)]
        [String]$password   , 
        [Parameter(Mandatory=$true)]
        $username  
    )

    $password   = $password | ConvertTo-SecureString -asPlainText -Force
    $credential = New-Object System.Management.Automation.PSCredential($username,$password)

    return $credential

} #End Function

 Function Create-SecurePasswordFile              {

    <#
    .Synopsis
       Create-SecurePasswordFile   
    .DESCRIPTION
       Create-SecurePasswordFile 
    .EXAMPLE
       Create-SecurePasswordFile  -username "lab\administrator"   
    .EXAMPLE
       Create-SecurePasswordFile  -username "lab\administrator" -Exportfile "$env:USERPROFILE\documents\Credentials_$username.txt"
    #>
 
    [CmdletBinding()]
 
    Param(  
            [Parameter(Mandatory=$true)]
            $username ,

            [Parameter(Mandatory=$false)]
            $Exportfile = "$env:USERPROFILE\documents\Credentials_$username.txt"
    )

    Begin{  
        Write-host "Create Passwordfile"
    }

    Process{
        $credentials = Read-Host "$Username" -AsSecureString 
        $credentials | ConvertFrom-SecureString | out-file $Exportfile
    }
    
    End{
        Write-host "Passwordfile Created in $Exportfile"
    }
} #End Function
 Function Create-SecurePasswordHash              {
   <#
    .Synopsis
      Create-SecurePasswordHash 
    .DESCRIPTION
      Create-SecurePasswordHash 
    .EXAMPLE
      Create-SecurePasswordHash -username domainname\username   

    #>

    [CmdletBinding()]

    Param(  
        #Param 
        [Parameter(Mandatory=$true)]
        [String]$username             
    )

    Begin{   
        Write-host "Create Passwordfile"
    }

    Process{

        $credentials = Read-Host "$Username" -AsSecureString 
        $hash        = $credentials | ConvertFrom-SecureString 
    
    }
    
    End{
       Return $hash
    }

} #End Function

 Function Get-PasswordFromSecureFile             {

   <#
    .Synopsis
      Create-SecurePasswordHash 
    .DESCRIPTION
      Create-SecurePasswordHash 
    .EXAMPLE
        Get-PasswordFromSecureFile -importFile "$env:userprofile\documents\Password.xml" -username administrator
    .EXAMPLE
        Get-PasswordFromSecureFile -importFile "$env:userprofile\documents\password.txt" -username administrator  

    #>

    [CmdletBinding()]

    Param(
        #Param 
        [Parameter(Mandatory=$true)]
        [string]$importFile,

        #Param 
        [Parameter(Mandatory=$true)]
        [String]$username
    )

    Begin{
        If(!(test-path $importFile)){ 
            write-warning "Could not find $importfile"
            Break
        }
    }

    Process{  
    	    If ($ImportFile -like "*.txt"){
                $Password   = Get-Content $importFile
            }ElseIf ($ImportFile -like "*.xml"){
                $Password  = Import-Clixml $importFile
            }#EndIf
                        
            $password = $Password | ConvertTo-SecureString
            
            #Set credentials
            $credentials = New-object System.Management.Automation.PSCredential -ArgumentList $username, $password   
    }

    End{
            return $credentials
    }
} #End Function
 Function Get-PasswordFromHash                   {


    <#
    .Synopsis
          Create-SecurePasswordHash 
        .DESCRIPTION
          Create-SecurePasswordHash 
        .EXAMPLE
             Get-PasswordFromHash -Hash $hash -username  -username administrator
 

    #>

    [CmdletBinding()]

    Param(
           #Param 
           [Parameter(Mandatory=$true)]
           [string]$Hash,
           #Param 
           [Parameter(Mandatory=$true)]
           [String]$username
    )

    Begin{
        #$hash = Create-SecurePasswordHash -username adminmw
        #ConverSecurestringtoPLainPassword -secureString $password
    }

    Process{  
            # Convert Hash to SecureString
              $password = $Hash | ConvertTo-SecureString
            # Set credentialsk
              $credentials = New-object System.Management.Automation.PSCredential -ArgumentList $username, $password   
    }

    End{
            return $credentials
    }
} #End Function

Export-ModuleMember -Function *



