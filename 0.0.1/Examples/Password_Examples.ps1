﻿
##############################################################################
# Password Database
####################################################################################
#  BASICS Password
####################################################################################

  $password       = "V3ryS3cr3t!!"

  $SecurePassword = $password | ConvertTo-SecureString -asPlainText -Force
 
  $SecurePassword = Read-Host -Prompt "Add Password" -AsSecureString


  
# Convert Back to plain password
  $password       = "V3ryS3cr3t!!"
  $SecurePassword = $password | ConvertTo-SecureString -asPlainText -Force
  $BSTR           = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword)
  $PlainPassword  = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR) 
  
  Write-host "Your password is $PlainPassword" -ForegroundColor Green

# Convert Back to plain password via Module
  $SecurePassword = Read-Host -Prompt "Add Password" -AsSecureString
  convert-SecurestringtoPLainPassword  -secureString  $SecurePassword 

  $credential = get-credential -Message "test" -UserName "domain\username"

# Set Credential variable
  $Username       = "lab\admininistrator"
  $SecurePassword = Read-Host -Prompt "Add Password" -AsSecureString
  $credential     = New-Object System.Management.Automation.PSCredential($Username,$SecurePassword)
  
  #or
  $credential     = New-object System.Management.Automation.PSCredential -ArgumentList $Username,$SecurePassword   
  
# Get-Credential 
  $credential     = get-credential -Message "Add credentials" -username "lab\admininistrator"

# Convert Back to plain password
  Convert-SecurestringtoPLainPassword -secureString  ($credential.password)
  

# Secure. Safe your password secure
  $password       = "Y0urS3cr3T_LetsHash"
  $SecurePassword = $password | ConvertTo-SecureString -asPlainText -Force
  $Username       = "lab\admininistrator"
  $credential     = New-Object System.Management.Automation.PSCredential("Username",$SecurePassword)

  $credential     = get-credential -Message "Add credentials" -username "lab\admininistrator"
  $hash           = $credential.Password | ConvertFrom-SecureString 

  $hash           = "01000000d08c9ddf0115d11efbsjgh67tyuhwqkjb36c1030000000d76b88edf78469acb012032fa6fd9acce9a77cf4272205280"
  $SecurePassword = $hash   | ConvertTo-SecureString
  
# Export hash to text file  
  $hash | out-file C:\powershell\Passwordhash.txt 

# Import hash form text file
  $readhash       = get-content C:\powershell\Passwordhash.txt 
  $SecurePassword = $readhash  | ConvertTo-SecureString

# Convert Back to plain password
  Convert-SecurestringtoPLainPassword -secureString $SecurePassword

# Use The functions from the Module
  Create-SecurePasswordFile -username administrator -Exportfile C:\powershell\Passwordhash.txt 
  $credential = Get-PasswordFromSecureFile -importFile C:\powershell\Passwordhash.txt -username administrator
  ConverSecurestringtoPLainPassword -secureString   $credential.Password

  $hash = Create-SecurePasswordHash -username domainname\username
  $credential = Get-PasswordFromHash -Hash $hash -username domainname\username
  ConverSecurestringtoPLainPassword -secureString   $credential.Password

####################################################################################
#  AES Encryption
####################################################################################
 $password       = "V3rYSecr3t_w1thAES"
 $SecurePassword = $password | ConvertTo-SecureString -asPlainText -Force

# Create AES key 
  [int]$AESKeySize = 32
       $AESKey     = (New-Object Byte[] $AESKeySize)

# Create Random AES Key in length specified in $Key variable.
  [Security.Cryptography.RNGCryptoServiceProvider]::Create().GetBytes($AESKey)
  $AESKey

  $SecurePasswordinHash = ConvertFrom-SecureString -SecureString $SecurePassword -Key $AESKey

# Convert back 
  $reversedPassword =  $SecurePasswordinHash | 
                         ConvertTo-SecureString -key $AESKey | 
                           ForEach-Object {[Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($_))}
  $reversedPassword

# use the function 
  $AESKey      = new-AESKey -AESKeySize 16
  $HashedToken = Convertto-SecureHashAES -token "DitIsEenSecureToken" -tokenName "UnsealKey1" -AESKey $AESKey
  Convertfrom-SecureHashAES -Hash $($HashedToken.hash) -AESKey $AESKey

#---------------------------------------------------
# Extra : Secure the AES key aswell
#---------------------------------------------------
# Secure The AES Key with personal Hash

  #Create AES Key
  $AESKey               = new-AESKey -AESKeySize 16  
  #Convert AES to Hash 
  $AESKeyHash           = $($AESKey -join " ") | ConvertTo-SecureString -asPlainText -Force  | ConvertFrom-SecureString
  #save Hash to file
  $AESKeyHash | out-file C:\powershell\Secure_AES_hash.txt
  #read Hash from file
  $AESKeyHash           = get-content  C:\powershell\Secure_AES_hash.txt


# Load Password
  $password       = "Z33rGeheim_metAESHASH"
  $SecurePassword = $password | ConvertTo-SecureString -asPlainText -Force
  $securePassword = Convert-PLainpasswordtoSecurestring -token $password
# Convert Back AES Key for AES Key Hash
  $SecureStringToBSTR  = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($($AESKeyHash | ConvertTo-SecureString))
  $AESKey              = ([System.Runtime.InteropServices.Marshal]::PtrToStringAuto($SecureStringToBSTR)) -split (" ")  

# create a Password hash
  $SecurePasswordinHash = ConvertFrom-SecureString -SecureString $SecurePassword -Key $AESKey
  $SecurePasswordinHash
  #ConvertBAck to plain password
  Convertfrom-SecureHashAES -Hash $SecurePasswordinHash -AESKey $AESKey



####################################################################################
#  CredentialManager 
####################################################################################
# Install and download the CredentialManager Powershell Module.
Install-Module -name CredentialManager
 
#Create New Credentials
New-StoredCredential -Target Administrator -UserName Administrator -Password "ZeerGeheimPassword!"

# Get Credential
$Credential = Get-StoredCredential -Target Administrator
$Credential 
 
# Get Credential as Plaintext
$Plaintext = (Get-StoredCredential -Target Administrator -AsCredentialObject).Password
$Plaintext  

# get all saved Credential
Get-StoredCredential  
 
# Remove Credential
Remove-StoredCredential -Target Administrator
 
# Check if credential is removed
Get-StoredCredential  

####################################################################################
#  KEEPASS
####################################################################################
# GET PASSWORD form keepass
  $PathTokeepassDB      = "C:\powershell\TestDatabase.kdbx"
  $KeePassPassword      = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($(read-host -Prompt "Password of KeePASS ($i/3) : " -AsSecureString))
        
  Connectto-Keepass -PlainPassword $([System.Runtime.InteropServices.Marshal]::PtrToStringAuto($KeePassPassword))  -PathTokeepassDB $PathTokeepassDB 


  $EntryToFind   = "You're entry in Keepass"
# secure Password
  $Passwordfromkeypass = (get-KeePassPassword -PathToDB  $PathTokeepassDB `
                                              -EntryToFind $EntryToFind `
                                              -keepassPassword $([System.Runtime.InteropServices.Marshal]::PtrToStringAuto($KeePassPassword)) ).password | 
                                                Convertto-SecureString -asPlainText -Force
                                                                       
# Convert Back to plain password
  $BSTR             = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($Passwordfromkeypass)
  $revertedPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR) 
  $revertedPassword


  $x = get-KeePassPassword -PathToDB $PathTokeepassDB -EntryToFind Cursus_Powershell

  $y = $x.Password | Convertto-SecureString -asPlainText -Force

 # Safe an entry with password to Keepass
  $Entryname     = "Powershell"
  $EntryUsername = "Powershell"
  $entryPassword = "ThisIsAPasswordInKeePass"
  set-KeePassPassword -PathToDB $PathTokeepassDB `
                      -Entryname $Entryname `
                      -EntryUsername $EntryUsername `
                      -EntryPassword $entryPassword `
                      -EntryURL "www.google.com" `
                      -EntryNotes "testaccount" `
                      -force `
                      -keepassPassword $([System.Runtime.InteropServices.Marshal]::PtrToStringAuto($KeePassPassword))

  get-KeePassPassword -PathToDB $PathTokeepassDB `
                      -EntryToFind $Entryname `
                      -keepassPassword $([System.Runtime.InteropServices.Marshal]::PtrToStringAuto($KeePassPassword))


####################################################################################
#  Hashicorp Vault with API and psVault module
####################################################################################
# GitHUB Page 
& "C:\Program Files\Mozilla Firefox\firefox.exe" https://github.com/D2CIT/Hashicorp-Vault

#Download module and place in C:\Program Files\WindowsPowerShell\Modules
# - check if files are locked
    $Path  = "C:\Program Files\WindowsPowerShell\Modules\psvault" ;
    $files =  get-childitem -path $Path -recurse | where {$_.Attributes -eq "Archive" } ;
    foreach($file in $files){ 
         write-host " - Unblock $($file.FullName)" ; 
         Unblock-File -Path  $file.FullName 
    }#EndForeach

# note : examples how to use this module see examples folder in module. 
#  - "C:\Program Files\WindowsPowerShell\Modules\PSVault\1.0.1\Examples\01_install-vault.ps1"
#  - "C:\Program Files\WindowsPowerShell\Modules\PSVault\1.0.1\Examples\02_examples_API.ps1"

    
# Load VauLtobject for connection
  $vaultobject = Get-Vaultobject -Address "http://127.0.0.1:8200" -Token "s.q7h4Yhit1uwf3onsIOfZkzzO"  # example token.
    
# Set Vaultobject for connection to API 
    $VaultToken  = $env:VAULT_TOKEN
    $VaultObject = [pscustomobject]@{"uri" = "http://127.0.0.1:8200";"auth_header" = @{"X-Vault-Token" = $VaultToken }}

# Create  Secret Engine
    $SecretEngineName = "kv_powershell_2"
    $uri              = "$($vaultobject.uri)/v1/sys/mounts/$SecretEngineName" 
    $KV_version       = 2

    #API CAll
    $payload = "{
        `"type`": `"kv`",
        `"options`": {
            `"version`": `"$KV_version`"
        }
    }"
        
    Invoke-RestMethod -uri $uri -headers $($vaultObject.auth_header) -Method post -body $payload

    #use function from psVault module
    new-VaultSecretEngine -vaultobject $VaultObject -SecretEngineName $SecretEngineName

# Get KV Engine configuration
    $uri     = "$($vaultobject.uri)/v1/$SecretEngineName/config"
    Invoke-RestMethod -Uri $uri -Method get -Headers $VaultObject.auth_header  
    
# Set KV Engine configuration
    $uri     = "$($vaultobject.uri)/v1/$SecretEngineName/config"
    $payload = '{
        "max_versions": 5,
        "cas_required": false
    }'
    Invoke-RestMethod -Uri $uri -Method post -Headers $VaultObject.auth_header -body $Payload   

# Create Secret
    $SecretEngineName = $SecretEngineName
    $Application      = "vsphere_api"
    $environment      = "prd" 
    $secretPath       = "$Application/$environment"
    $uri_V1           = "$($vaultobject.uri)/v1/$SecretEngineName/$secretPath" 
    $uri_V2           = "$($vaultobject.uri)/v1/$SecretEngineName/data/$secretPath"
    #Secret to store
    $username         = "Administrator"
    $password         = "P@SSW0rd12!"
    $Server           = "srv01"
    
    #API Call
    $data ="{`"data`": { `"username`": `"$username`", `"password`": `"$Password`",`"server`": `"$server`" }}"
    Invoke-RestMethod -uri $uri_v2 -headers $($vaultObject.auth_header) -Method post -body $data



   # use function from psVault module
    set-VaultSecret -VaultObject $vaultobject -secretEnginename $SecretEngineName `
                                              -SecretPath $secretPath `
                                              -username $username `
                                              -password $Password `
                                              -environment $environment `
                                              -tag $tag `
                                              -server $server
    
     
    get-VaultSecret -VaultObject $vaultobject -secretEnginename $SecretEngineName -SecretPath $secretPath 

# get Secret
    #API Call
    $result = Invoke-RestMethod -uri $uri_v2  -headers $($vaultObject.auth_header) -Method get 
    $object = New-Object -TypeName psobject -Property @{
                username       = $result.data.data.username
                password       = $result.data.data.password
                server         = $result.data.data.server 
                environment    = $result.data.data.environment
                tag            = $result.data.data.Tag
                created_time   = $result.data.metadata.created_time 
                deletion_time  = $result.data.metadata.deletion_time
                destroyed      = $result.data.metadata.destroyed
                version        = $result.data.metadata.version
                request_id     = $result.request_id 
                lease_duration = $result.lease_duration
                renewable      = $result.renewable
                wrap_info      = $result.wrap_info
                warnings       = $result.warnings
                auth           = $result.auth
            }#EndObject
    $object

    #use function from psVault module
    get-VaultSecret -VaultObject $vaultobject -secretEnginename $SecretEngineName -SecretPath $secretPath

  # Set PSCredential with Username and Password from Hashicorp Vault
    $Secret         = get-VaultSecret -VaultObject $vaultobject -secretEnginename $SecretEngineName -SecretPath $secretPath
    $username       = $Secret.username
    $password       = $Secret.password | ConvertTo-SecureString -asPlainText -Force
    $credential     = New-object System.Management.Automation.PSCredential -ArgumentList $username, $password   


# Create Policy
    $vaultpath  = "C:\vault"
    $PolicyPath = "$VaultPath\config\policy"
    $PolicyName = "p_" + $SecretEngineName + "_" + $Application + "_" + $environment
    $PolicyFile = "$PolicyPath\$PolicyName.hcl"

    $capabilities =  "[`"read`",`"list`"]" 
    $path   = "$SecretEngineName/*"
    "path `"$Path`" {"                  | out-file -Encoding ascii -FilePath $PolicyFile
    "    capabilities = $capabilities " | out-file -Encoding ascii -FilePath $PolicyFile -append
    "}"                                 | out-file -Encoding ascii -FilePath $PolicyFile -append                             
    
    $capabilities =  "[`"read`",`"list`",`"create`",`"update`",`"delete`"]" 
    $path   = "$SecretEngineName/$Application/$environment/*"
    "path `"$Path`" {"                  | out-file -Encoding ascii -FilePath $PolicyFile -append
    "    capabilities = $capabilities " | out-file -Encoding ascii -FilePath $PolicyFile -append
    "}"                                 | out-file -Encoding ascii -FilePath $PolicyFile -append 

     vault policy write $PolicyName  $PolicyFile


# List policies
    $uri    = "$($vaultobject.uri)/v1/sys/policy" 
    $result = Invoke-RestMethod -Uri $uri -Method get -Headers $VaultObject.auth_header
    $policies = $result.policies
    $policies
