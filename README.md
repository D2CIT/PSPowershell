# PSPowershell

Powershell module for passwords. See blog : https://d2c-it.nl/2019/12/03/powershell-passwords/

Passwords in scripts. What are the options and how do you apply them? In this blog I will show a number of possibilities of using passwords within  PowerShell. Many administrators put their passwords in the body of their scripts. Just for testing ….
Passwords unencrypted in scripts is a terrifying thing. It can be forgotten or worse.
It’s easy to hash your password so why should we pace passwords in scripts even if it is for testing purposes.